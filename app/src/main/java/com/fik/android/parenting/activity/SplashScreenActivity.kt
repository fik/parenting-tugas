package com.fik.android.parenting.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.fik.android.parenting.R
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit
import android.content.Intent
import android.view.View
import kotlinx.android.synthetic.main.activity_splash.*

class SplashScreenActivity: AppCompatActivity() {

    private val compositeDisposable = CompositeDisposable()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        compositeDisposable.add(Completable.timer(1, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
            .subscribe(this::showProgressBar))

        compositeDisposable.add(Completable.timer(3, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
            .subscribe(this::intentTo))
    }

    private fun intentTo() {
        hideProgressBar()
        val i = Intent(applicationContext, LoginActivity::class.java)
        startActivity(i)
        finish()
    }

    private fun showProgressBar() {
        rl_loading.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        rl_loading.visibility = View.GONE
    }

    override fun onDestroy() {
        //dispose
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
        super.onDestroy()
    }

}