package com.fik.android.parenting.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.fik.android.parenting.R
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        login.setOnClickListener {validateText()}
    }

    private fun validateText() {
        if(!username.text.toString().isEmpty() && !password.text.toString().isEmpty()){
            val i = Intent(applicationContext, LandingActivity::class.java)
            startActivity(i)
            finish()
        }else{
            Toast.makeText(applicationContext, "Username/password kosong", Toast.LENGTH_SHORT).show()
        }
    }
}