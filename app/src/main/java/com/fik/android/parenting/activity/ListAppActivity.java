package com.fik.android.parenting.activity;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import com.fik.android.parenting.R;

/**
 * Created by Mochamad Taufik on 11-Jan-19.
 * Email   : thidayat13@gmail.com
 */
public class ListAppActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private RecyclerView rvInstalled;

    private PackageManager mPackageManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_app);

        toolbar         = findViewById(R.id.toolbar);
        rvInstalled     = findViewById(R.id.rv_installed);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Lock App");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        rvInstalled.setHasFixedSize(false);
        rvInstalled.setLayoutManager(new LinearLayoutManager(this));

        getListApp();
    }

    private void getListApp() {
    }

}
