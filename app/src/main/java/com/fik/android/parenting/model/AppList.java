package com.fik.android.parenting.model;

/**
 * Created by Mochamad Taufik on 11-Jan-19.
 * Email   : thidayat13@gmail.com
 */

public class AppList {

    private String packageName;
    private String appName;
    private byte[] appInfo;
    private Boolean isSysApp;

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public byte[] getAppInfo() {
        return appInfo;
    }

    public void setAppInfo(byte[] appInfo) {
        this.appInfo = appInfo;
    }

    public Boolean getIsSysApp() {
        return isSysApp;
    }

    public void setIsSysApp(Boolean isSysApp) {
        this.isSysApp = isSysApp;
    }
}

