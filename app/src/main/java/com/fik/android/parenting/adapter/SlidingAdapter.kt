package com.fik.android.parenting.adapter

import android.content.Context
import android.os.Parcelable
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.fik.android.parenting.R
import kotlinx.android.synthetic.main.item_slide_img.view.*
import java.util.*

/**
 * Created by Mochamad Taufik on 09-Jan-19.
 * Email   : thidayat13@gmail.com
 */

class SlidingAdapter(private val context: Context, private val IMAGES: ArrayList<Int>) : PagerAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    override fun getCount(): Int {
        return IMAGES.size
    }

    override fun instantiateItem(view: ViewGroup, position: Int): Any {
        val imageLayout = inflater.inflate(R.layout.item_slide_img, view, false)

        Glide.with(context).load(IMAGES[position]).into(imageLayout.image)

        view.addView(imageLayout, 0)

        return imageLayout
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun restoreState(state: Parcelable?, loader: ClassLoader?) {}

    override fun saveState(): Parcelable? {
        return null
    }
}
