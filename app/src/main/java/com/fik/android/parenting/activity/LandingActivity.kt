package com.fik.android.parenting.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.fik.android.parenting.R
import com.fik.android.parenting.adapter.SlidingAdapter
import kotlinx.android.synthetic.main.activity_landing.*
import java.util.*

class LandingActivity : AppCompatActivity() {

    private val ImagesArray = ArrayList<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_landing)

        //populate data
        setDataImage()

        //set data to adapter
        view_pager.adapter = SlidingAdapter(applicationContext, ImagesArray)
        indicator.setViewPager(view_pager)

        //onclick
        rl_menu1.setOnClickListener {intentToListApp()}
        rl_menu2.setOnClickListener {intentToPlayTime()}
        rl_menu3.setOnClickListener {intentToMaps()}
        rl_menu4.setOnClickListener {Toast.makeText(applicationContext,"Menu 4", Toast.LENGTH_LONG).show()}
        btn_start.setOnClickListener {Toast.makeText(applicationContext,"Start", Toast.LENGTH_LONG).show()}
    }

    private fun intentToListApp() {
        val i = Intent(applicationContext, ListAppActivity::class.java)
        startActivity(i)
    }

    private fun intentToPlayTime() {
        val i = Intent(applicationContext, WaktuBermainActivity::class.java)
        startActivity(i)
    }

    private fun intentToMaps(){
        val i = Intent(applicationContext, MapsActivity3::class.java)
        startActivity(i)
    }

    private fun setDataImage() {
        //loop data dummy
        for (i in 1..3) {
            ImagesArray.add(R.drawable.ic_picture)
        }
    }
}
